// Get Selenium and the drivers
var seleniumServer = require('selenium-server');
var chromedriver = require('chromedriver');
var geckodriver = require('geckodriver');

var config = {
  src_folders: [
    // Folders with tests
    'tests'
  ],
  output_folder: 'reports', // Where to output the test reports
  page_objects_path: [
    'pages'
  ],
  custom_commands_path: 'custom',
  selenium: {
    // Information for selenium, such as the location of the drivers ect.
    start_process: true,
    server_path: "./node_modules/selenium-server/lib/runner/selenium-server-standalone-3.141.59.jar",
    port: 4444, // Standard selenium port
    cli_args: {
      "webdriver.chrome.driver" : "./node_modules/chromedriver/lib/chromedriver/chromedriver",
      "webdriver.gecko.driver" : "./node_modules/geckodriver"
    }
  },
  test_workers: {
    // This allows more then one browser to be opened and tested in at once
    enabled: true,
    workers: 'auto'
  },
  test_settings: {
    default: {
      "launch_url": "http://testing-ground.scraping.pro/login",
      screenshots: {
        enabled: false
      },
      globals: {
        // How long to wait (in milliseconds) before the test times out
        waitForConditionTimeout: 5000,
        "ADMIN_USERNAME": "admin",
        "ADMIN_PASSWORD": "12345",
      },
      desiredCapabilities: {
        // The default test driver
        browserName: 'chrome',
        javascriptEnabled: true,
        acceptSslCerts: true,
        nativeEvents: true
      }
    },
    "dev": {
        launch_url: "http://testing-ground.scraping.pro/login",
    },
    "qa": {
        launch_url: "http://testing-ground.scraping.pro/login",
    },
    "stage": {
        launch_url: "http://testing-ground.scraping.pro/login",
    },
    // Here, we give each of the browsers we want to test in, and their driver configuration
    chrome: {
      desiredCapabilities: {
        browserName: 'chrome',
        javascriptEnabled: true,
        acceptSslCerts: true,
        nativeEvents: true,
        "chromeOptions" : {
          "w3c" : false,
          "args" : ["incognito"]
        }
      }
    },
    firefox: {
      desiredCapabilities: {
        browserName: 'firefox',
        javascriptEnabled: true,
        acceptSslCerts: true,
        nativeEvents: true
      }
    },
    safari: {
      desiredCapabilities: {
        browserName: 'safari',
        javascriptEnabled: true,
        acceptSslCerts: true,
        nativeEvents: true
      }
    }
  }
};

module.exports = config;
