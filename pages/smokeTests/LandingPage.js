module.exports = {
    url: '',

    elements:{
        GoBackLink: {
            selector: '#case_login > a:nth-child(2)'
        }
    },

    commands: [
        {
            ClickGoBackLink: function () {
                this
                .click('@GoBackLink')
                .waitForElementPresent('#usr')
                .assert.elementNotPresent('@GoBackLink')
                return this.api
            }

        }
    ]
};
