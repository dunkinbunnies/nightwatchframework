module.exports = {
    'Verify login': function(browser) {
      browser
        .login(browser.launchUrl,browser.globals.ADMIN_USERNAME,browser.globals.ADMIN_PASSWORD)
        .end()
    },

    'Verify Go Back Link': function(browser) {
      const GoBack = browser.page.smokeTests.LandingPage();

      browser
        .login(browser.launchUrl,browser.globals.ADMIN_USERNAME,browser.globals.ADMIN_PASSWORD)
        GoBack.ClickGoBackLink();
        browser.end()
    }
  }
