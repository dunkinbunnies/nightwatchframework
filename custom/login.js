exports.command = function(url, username, password) {
    this
        .url(url)
        .waitForElementPresent("input[name='usr']",10000)
        .setValue("input[name='usr']", username)
        .setValue("input[name='pwd']", password)
    this
        .submitForm("input[type='submit']")
        .assert.visible("h3.success")
        return this;
};
